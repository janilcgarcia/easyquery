<?php

namespace EasyQuery;

interface DBQuery
{
  public function asArray();
  public function asAssocArray();
  public function field($field);
}
