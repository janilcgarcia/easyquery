<?php

namespace EasyQuery;

class EasyQueryConfig
{
  private const engines = [
    'pgsql' => '\EasyQuery\DBEngines\PostgreSQL',
  ];

  private $dbEngine;

  public function __construct($config)
  {
    $engine = $config['engine'];
    $this->dbEngine = (self::engines[$engine])::makeFromConfig($config);
  }

  public function getDatabaseEngine(): DBEngine
  {
    return $this->dbEngine;
  }
}
