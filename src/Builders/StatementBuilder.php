<?php

namespace Builders;

class StatementBuilder {
  public function __construct() {
    $this->sql = '';
    $this->params = [];
  }

  public function addRaw($string) {
    $this->sql .= $string;
  }

  public function addParameter($param) {
    $p = '$' . count($this->params);
    $this->params[] = $param;

    return $p;
  }

  public function resolveInterpolate($expressions, $separator) {
    if (!($separator instanceof Expression)) {
      $separator = new SymbolExpression($separator);
    }

    $first = true;

    foreach ($expressions as $expr) {
      if (!$first) {
        $separator->resolve($this);
      }

      $expr->resolve($this);
      $first = false;
    }
  }
}
