<?php

namespace EasyQuery\Builders;

trait WhereBuilder {
  protected $where;

  public function where($expr) {
    if ($this->where) {
      $this->where = new InfixOpExpression("AND", array($this->where, $expr));
    } else {
      $this->where = $expr;
    }

    return $this;
  }
}
