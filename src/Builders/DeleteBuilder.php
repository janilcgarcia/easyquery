<?php

namespace EasyQuery\Builders;

use EasyQuery\Expression as E;

class DeleteBuilder
{
  use WhereBuilder;

  private $table;
  private $engine;

  public function __construct($engine, $table) {
    $this->engine = $engine;
    $this->table = E\id($table);
  }

  public function build() {
    $params = [];
    $sql = [];

    $sql[] = sprintf('DELETE FROM %s', $this->table->resolve($this->engine));
    array_merge($params, $this->table->getParameters($this->engine));

    if ($this->where)
    {
      $sql[] = sprintf(' WHERE %s', $this->where->resolve($this->engine));
      array_merge($params, $this->where->getParameters($this->engine));
    }

    return array(implode($sql, ''), $params);
  }

  public function execute() {
    $sql = $this->build();

    return $this->engine->run($sql[0], $sql[1]);
  }
}
