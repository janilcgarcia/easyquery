<?php

namespace EasyQuery\Builders;

use \EasyQuery\Expression as E;

class SelectBuilder
{
  use WhereBuilder;

  private $engine;

  private $table;
  private $fields;
  private $class;

  private $orderExprs;
  private $limit;
  private $offset;

  public function __construct($engine, $class)
  {
    $this->engine = $engine;
    $this->class = $class;
    $this->table = $this->class->getTable();
    $this->fields = E\group(false, ...$this->class->getColumns());

    $this->where = null;
    $this->orderExprs = [];
    $this->limit = 0;
    $this->offset = 0;
  }

  public function orderBy($exprs)
  {
    $this->orderExprs = array_merge($this->orderExprs, $exprs);

    return $this;
  }

  public function limit($limit, $offset = null)
  {
    $this->limit = $limit;
    $this->offset = $offset;

    return $this;
  }

  public function build()
  {
    $sql = [];
    $params = [];

    $sql[] = vsprintf('SELECT %s FROM %s',
        array($this->fields->resolve($this->engine), $this->table));

    array_merge($params, $this->fields->getParameters($this->engine));


    if ($this->where)
    {
      $sql[] = vsprintf('WHERE %s', $this->where->resolve($this->engine));
      array_merge($params, $this->where->getParameters($this->engine));
    }

    if ($this->orderExprs)
    {
      $order = group(false, $this->orderExprs);
      $sql[] = vsprintf('ORDER BY %s', $order->resolve($this->engine));
      array_merge($params, $order->getParameters($this->engine));
    }

    if ($this->limit && $this->limit > 0)
    {
      $sql[] = vsprintf('LIMIT %s', $this->limit);

      if (!is_null($this->offset) && $this->offset >= 0)
      {
        $sql[] = vsprintf('OFFSET %s', $this->offset);
      }
    }

    return array(implode($sql, ' '), $params);
  }

  public function all() {
    $stmt = $this->build();

    $result = $this->engine->query($stmt[0], $stmt[1]);

    if (!$result) {
      throw new Exception('Query Error');
    }

    $results = [];

    while ($assoc = $result->asAssocArray($result)) {
      $results[] = $this->class->make($assoc, false);
    }

    return $results;
  }

  public function one() {
    $stmt = $this->build();
    $result = $this->engine->query($stmt[0], $stmt[1]);

    if (!$result) {
      throw new \Exception('Query error');
    }

    $assoc = $result->asAssocArray();

    return $this->class->make($assoc, false);
  }

  public function first() {
    $stmt = $this->build();

    if ($assoc = pg_fetch_assoc($result)) {
      return $this->class->newInstance($assoc, false);
    }

    return null;
  }
}
