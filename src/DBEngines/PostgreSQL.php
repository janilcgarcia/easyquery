<?php

namespace EasyQuery\DBEngines;

use EasyQuery\DBEngine;
use EasyQuery\DBQuery;

class PostgreSQL implements DBEngine
{
  private $connection;

  private function __construct($connection)
  {
    $this->connection = $connection;
  }

  public static function makeFromConfig($config)
  {
    $params = [];

    foreach ($config as $key => $value)
    {
      if ($key === 'engine')
        continue;

      if (!$value)
        continue;

      if ($value === '' || preg_match('/\s/', $value)) {
        $value = '\'' . str_replace('\'', '\\\'', $value) . '\'';
      }

      $params[] = "$key=$value";
    }

    return new self(pg_connect(implode($params, ' ')));
  }

  public function query($sql, $parameters): DBQuery
  {
    $result = pg_query_params($this->connection, $sql, $parameters);

    if (!$result)
      throw new Exception('Failed to execute query');

    return new PostgreSQLQuery($result);
  }

  public function run($sql, $parameters): int
  {
    $result = pg_query_params($this->connection, $sql, $parameters);

    if (!$result)
      throw new Exception('Failed to execute query');

    return pg_affected_rows($result);
  }

  public function escapeIdentifier($id): string
  {
    return pg_escape_identifier($this->connection, $id);
  }

  public function escapeLiteral($literal): string
  {
    return pg_escape_literal($this->connection, $literal);
  }
}
