<?php

namespace EasyQuery\DBEngines;

use EasyQuery\DBQuery;

class PostgreSQLQuery implements DBQuery
{
  private $result;

  public function __construct($result)
  {
    $this->result = $result;
  }

  public function asArray()
  {
    return \pg_fetch_array($this->result);
  }

  public function asAssocArray()
  {
    return \pg_fetch_assoc($this->result);
  }

  public function field($f)
  {
    return \pg_fetch_result($this->result, $f);
  }
}
