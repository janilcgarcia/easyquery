<?php

namespace EasyQuery;

class Entity {
  private $fields;
  private $values;
  private $dirtyFields;
  private $new;

  public function __construct($initial = [], $new = true) {
    $class = new \ReflectionClass($this);

    $this->fields = $class->getConstant('fields');

    $initial = $this->fromDatabase($initial);

    if (!$new)
    {
      $this->values = $initial;
      $this->dirtyFields = [];
    }
    else
    {
      $this->values = [];
      $this->dirtyFields = $initial;
    }

    $this->new = $new;
  }

  private function fromDatabase($assoc)
  {
    $out = [];

    foreach ($assoc as $key => $value)
    {
      $bits = \explode('_', $key);
      $bit1 = \array_shift($bits);
      $bits = \array_map('\ucfirst', $bits);
      \array_unshift($bits, $bit1);

      $out[implode($bits, '')] = $assoc[$key];
    }

    return $out;
  }

  public function get($key) {
    if (isset($this->dirtyFields[$key])) {
      return $this->dirtyFields[$key];
    }

    if (in_array($key, $this->fields)) {
      return isset($this->values[$key]) ? $this->values[$key] : null;
    }

    throw new \Exception("Attribute '$key' does not belong to entity");
  }

  public function set($key, $value) {
    if (in_array($key, $this->fields)) {
      $this->dirtyFields[$key] = $value;
      return;
    }

    throw new \Exception("Attribute '$key' does not belong to model");
  }

  public function __call($name, $args)
  {
    $ms = array();
    if (preg_match('/set(.*)/', $name, $ms)) {
      $propName = strtolower($ms[1][0]) . substr($ms[1], 1);

      $this->set($propName, $args[0]);
      return;
    }

    if (preg_match('/get(.*)/', $name, $ms)) {
      $propName = strtolower($ms[1][0]) . substr($ms[1], 1);

      return $this->get($propName);
    }

    throw new \Exception("Unimplemented function: {$name}");
  }

  public function clearFields() {
    foreach ($this->dirtyFields as $key => $value) {
      $this->values[$key] = $value;
    }

    $this->dirtyFields = [];

    $this->new = false;
  }

  public function getDirty() {
    return $this->dirtyFields;
  }

  public function isDirty() {
    return !empty($this->dirtyFields);
  }

  public function isNew() {
    return $this->new;
  }

  public static function props()
  {
    return new EntityProps(get_called_class());
  }
}
