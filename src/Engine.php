<?php

namespace EasyQuery;

use EasyQuery\Expression as E;

class Engine {
  private $config, $db;

  public function __construct($config = EASYQUERY_GLOBAL_CONFIG) {
    $this->config = $config;

    if (!($this->config instanceof EasyQueryConfig)) {
      $this->config = new EasyQueryConfig($config);
    }

    $this->db = $this->config->getDatabaseEngine();
  }

  public function select($props) {
    return new Builders\SelectBuilder($this->db, $props);
  }

  public function save($model) {
    if ($model->isNew()) {
      $this->insert($model);
    } else if ($model->isDirty()) {
      $this->update($model);
    }
  }

  private function insert($model) {
    $props = $model->props();
    $columns = $props->getColumnDict();
    $table = $props->getTable();

    if ($props->isAutogenPK()) {
      unset($columns[$props->getPK()]);
    }

    $values = [];
    $columnValues = [];

    foreach ($columns as $key => $value)
    {
      $columnValues[] = $value->dropQualifier();
      $values[] = E\literal($model->get($key));
    }

    $values = E\group(true, ...$values);
    $columnValues = E\group(true, ...$columnValues);

    $sql = vsprintf('INSERT INTO %s%s VALUES %s RETURNING %s',
        array($table, $columnValues->resolve($this->db),
          $values->resolve($this->db), $props->getPK()));

    $params = [];
    array_merge($params, $columnValues->getParameters($this->db),
      $values->getParameters($this->db));

    $model->set($props->getPK(), $this->db->query($sql, $params)->field(0));
    $model->clearFields();
  }

  public function update($model) {
    $props = $model->props();
    $table = E\id($props->getTable());
    $pk = $props->getPK();
    $dirty = $model->getDirty();

    $columns = [];
    foreach ($dirty as $key => $value)
    {
      $columns[] = $props->$key->dropQualifier()->eq($value);
    }
    $columns = E\group(false, ...$columns);

    $condition = $props->$pk->eq($model->get($pk));

    $sql = sprintf('UPDATE %s SET %s WHERE %s', $table->resolve($this->db),
        $columns->resolve($this->db), $condition->resolve($this->db));

    $params = [];
    array_merge($params, $columns->getParameters($this->db),
        $condition->getParameters($this->db));

    $this->db->run($sql, $params);

    $model->clearFields();
  }

  public function delete($props) {
    return new Builders\DeleteBuilder($this->db, $props->getTable());
  }

  public function deleteEntity($entity) {
    $props = $entity->props();
    $pk = $props->getPK();

    $this->delete($props)->where($props->$pk->equals($entity->$pk))->execute();
  }

  public function pureQuery($props, $sql, $params = []) {
    $result = pg_query_params($sql);

    if (!$result) {
      throw new Exception('Pure query error. SQL Code: "' . $sql . '"');
    }

    $list = [];
    while ($row = pg_fetch_assoc($result)) {
      $list[] = $props->make($row);
    }

    return $list;
  }

  public function pureExecute($sql, $params = []) {
    $r = pg_query_params($sql, $params);

    if (!$r) {
      throw new Exception('Pure query execution error. SQL Code: "' . $sql . '"');
    }
  }
}
