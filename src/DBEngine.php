<?php

namespace EasyQuery;

interface DBEngine
{
  public function query($sql, $parameters): DBQuery;
  public function run($sql, $parameters): int;
  
  public function escapeIdentifier($id): string;
  public function escapeLiteral($literal): string;
}
