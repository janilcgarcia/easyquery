<?php

namespace EasyQuery\Expression;

abstract class Value implements Expression
{
  use ValueOpTrait;
  use ComparisionOpTrait;
}
