<?php

namespace EasyQuery\Expression;

class OP
{
  public static function AND($expr1, $expr2, ...$exprs) {
    array_unshift($exprs, $expr1, $expr2);
    return criteria('AND', ...$exprs);
  }

  public static function OR($expr1, $expr2, ...$exprs) {
    array_shift($exprs, $expr1, $expr2);
    return criteria('OR', ...$exprs);
  }

  public static function NOT($expr) {
    return criteria('NOT', $expr);
  }

  public const precendenceTable = [
    ['OR'], ['AND'], ['NOT'], ['IS'],
    ['<', '>', '<=', '>=', '=', '<>'],
    ['BETWEEN', 'IN', 'LIKE', 'ILIKE', 'SIMILAR'],
    ['OTHER'],
    ['+', '-'],
    ['*', '/', '%'],
    ['^'],
  ];

  private static function invertTable($table)
  {
    $out = [];

    foreach ($table as $key => $row)
    {
      foreach ($row as $cell)
      {
        $out[$cell] = $key;
      }
    }

    return $out;
  }

  public static function invPrecTable()
  {
    return self::invertTable(self::precendenceTable);
  }
}
