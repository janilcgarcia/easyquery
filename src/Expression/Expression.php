<?php

namespace EasyQuery\Expression;

use EasyQuery\DBEngine;

interface Expression
{
  public function resolve(DBEngine $dbEngine): string;
  public function getParameters(DBEngine $dbEngine): array;
}
