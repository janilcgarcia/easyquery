<?php

namespace EasyQuery\Expression;

use EasyQuery\DBEngine;

class Literal extends Value
{
  private $value;

  public function __construct($value)
  {
    $this->value = $value;
  }

  public function resolve(DBEngine $dbEngine): string
  {
    if (is_null($this->value))
      return 'NULL';
    return $dbEngine->escapeLiteral($this->value);
  }

  public function getParameters(DBEngine $dbEngine): array
  {
    return array($this->value);
  }
}
