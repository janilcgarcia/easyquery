<?php

namespace EasyQuery\Expression;

class FunctionExpr extends Value
{
  private $name;
  private $args;

  public function __construct($name, ...$args)
  {
    $this->name = $name;
    $this->args = group(true, $args);
  }

  public function resolve($dbEngine)
  {
    return "{$this->name}{$this->args->resolve($dbEngine)}";
  }

  public function getParameters($dbEngine)
  {
    return $this->args->getParameters($dbEngine);
  }
}
