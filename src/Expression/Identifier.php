<?php

namespace EasyQuery\Expression;

use EasyQuery\DBEngine;

class Identifier extends Value
{
  private $components;

  public function __construct(...$components)
  {
    $this->components = $components;
  }

  public function resolve(DBEngine $dbEngine): string
  {
    return implode(array_map(array($dbEngine, 'escapeIdentifier'),
        $this->components), '.');
  }

  public function getParameters(DBEngine $dbEngine): array
  {
    return array();
  }

  public function dropQualifier(): self
  {
    return id(\end($this->components));
  }
}
