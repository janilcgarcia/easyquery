<?php

namespace EasyQuery\Expression;

class CompositeValue extends Criteria
{
  use ComparisionOpTrait;
  use ValueOpTrait;
}
