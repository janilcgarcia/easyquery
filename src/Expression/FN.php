<?php

namespace EasyQuery\Expression;

class FN
{
  public static function __callStatic($name, $args)
  {
    return fn($name, ...array_map('literal', $args));
  }
}
