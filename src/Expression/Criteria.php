<?php

namespace EasyQuery\Expression;

use EasyQuery\DBEngine;

class Criteria implements Expression
{
  private $operator;
  private $components;
  private $level;

  public function __construct($operator, ...$components)
  {
    $this->operator = $operator;
    $this->level = OP::invPrecTable()[$this->operator] || OP::invPrecTable()['OTHER'];
    $this->components = [];

    foreach ($components as $c)
    {
      if ($c instanceof Criteria && $c->level < $this->level)
        $this->components[] = group(true, $c);

      $this->components[] = $c;
    }
  }

  public function resolve(DBEngine $dbEngine): string
  {
    if (count($this->components) === 1)
    {
      return $this->operator . ' ' . $this->components[0]->resolve($dbEngine);
    }

    $resolved = [];

    foreach ($this->components as $c)
      $resolved[] = $c->resolve($dbEngine);

    return implode($resolved, ' ' . $this->operator . ' ');
  }

  public function getParameters(DBEngine $dbEngine): array
  {
    $params = array();

    foreach ($this->components as $c)
      array_merge($params, $c->getParameters($dbEngine));

    return $params;
  }
}
