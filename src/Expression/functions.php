<?php

namespace EasyQuery\Expression;

function criteria($op, ...$cs)
{
  return new Criteria($op, ...$cs);
}

function group($p, ...$cs)
{
  return new Group($p, ...$cs);
}

function id(...$id)
{
  return new Identifier(...$id);
}

function literal($l)
{
  if ($l instanceof Expression)
    return $l;

  return new Literal($l);
}

function compValue($op, ...$literals)
{
  return new CompositeValue($op, ...$literals);
}

function fn($name, ...$args)
{
  return new FunctionExpr($name, ...$args);
}
