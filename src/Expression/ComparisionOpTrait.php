<?php

namespace EasyQuery\Expression;

trait ComparisionOpTrait
{
  public function eq($o)
  {
    return criteria('=', $this, literal($o));
  }

  public function notEq($o)
  {
    return criteria('<>', $this, literal($o));
  }

  public function less($o)
  {
    return criteria('<', $this, literal($o));
  }

  public function more($o)
  {
    return criteria('>', $this, literal($o));
  }

  public function lessEq($o)
  {
    return criteria('<=', $this, literal($o));
  }

  public function moreEq($o)
  {
    return criteria('>=', $this, literal($o));
  }
}
