<?php

namespace EasyQuery\Expression;

use EasyQuery\DBEngine;

class Group implements Expression
{
  private $parenthesis;
  private $components;

  public function __construct($parenthesis, ...$components)
  {
    $this->parenthesis = $parenthesis;
    $this->components = $components;
  }

  public function resolve(DBEngine $dbEngine): string
  {
    $s = '%s';

    if ($this->parenthesis)
      $s = '(%s)';

    $resolved = [];

    foreach ($this->components as $c)
      $resolved[] = $c->resolve($dbEngine);

    return vsprintf($s, implode($resolved, ', '));
  }

  public function getParameters(DBEngine $dbEngine): array
  {
    $params = [];

    foreach ($this->components as $c)
      array_merge($params, $c->getParameters($dbEngine));

    return $params;
  }
}
