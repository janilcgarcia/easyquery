<?php

namespace EasyQuery\Expression;

trait ValueOpTrait
{
  public function plus($other)
  {
    $other = literal($other);

    return compValue('+', $this, $other);
  }

  public function minus($other)
  {
    $other = literal($other);

    return compValue('-', $this, $other);
  }

  public function times($other)
  {
    $other = literal($other);

    return compValue('*', $this, $other);
  }

  public function div($other)
  {
    $other = literal($other);

    return compValue('/', $this, $other);
  }

  public function mod($other)
  {
    $other = literal($other);

    return compValue('%', $this, $other);
  }

  public function custom($op, $other)
  {
    $other = literal($other);

    return compValue($op, $this, $other);
  }

  public function like($other)
  {
    $other = literal($other);

    return compValue('LIKE', $this, $other);
  }

  public function ilike($other)
  {
    $other = literal($other);
    return compValue('ILIKE', $this, $other);
  }

  public function startsWith($literal)
  {
    return $this->like($literal . '%');
  }

  public function endsWith($literal)
  {
    return $this->like('%' . $literal);
  }

  public function contains($literal)
  {
    return $this->like("%$literal%");
  }

  public function istartsWith($literal)
  {
    return $this->ilike($literal . '%');
  }

  public function iendsWith($literal)
  {
    return $this->ilike('%' . $literal);
  }

  public function icontains($literal)
  {
    return $this->ilike("%$literal%");
  }
}
