<?php

namespace EasyQuery;

use EasyQuery\Expression as E;

class EntityProps {
  private $table;
  private $class;
  private $fields;
  private $pk;
  private $autogenPk;
  private $columns;

  public function __construct($class) {
    $this->class = new \ReflectionClass($class);
    $this->table = $this->class->getConstant('table');
    $this->fields = $this->class->getConstant('fields');
    $this->pk = $this->class->hasConstant('pk')
        ? $this->class->getConstant('pk') : 'id';
    $this->autogenPk = $this->class->hasConstant('autogenPK')
        ? $this->class->getConstant('autogenPK') : true;
    $this->columns = [];

    foreach ($this->fields as $field) {
      $this->columns[$field] = E\id($this->table, toSnakeCase($field));
    }
  }

  public function make(...$params) {
    return $this->class->newInstance(...$params);
  }

  public function __get($name) {
    if (in_array($name, $this->fields)) {
      return $this->columns[$name];
    }

    throw new Exception('Field doesn\'t exist in model');
  }

  public function getTable() {
    return $this->table;
  }

  public function getFields() {
    return $this->fields;
  }

  public function getClass() {
    return $this->class;
  }

  public function getPK() {
    return $this->pk;
  }

  public function isAutogenPK() {
    return $this->autogenPk;
  }

  public function getColumns()
  {
    return array_values($this->columns);
  }

  public function getColumnDict()
  {
    return $this->columns;
  }
}
